#!/bin/bash

# Standard-UID und GID
default_uid=500
default_gid=500

# Farbdefinitionen
yellow='\e[33m'
red='\e[31m'
green='\e[32m'
blue='\e[34m'
reset='\e[0m'

# Funktion, um Serviceuser-Erstellung abzufragen
confirm_creation() {
    echo -n -e "${blue}|${reset} ${blue}Serviceuser erstellen?${reset} (J für Ja, beliebige Taste für Nein): "
    read -r confirm
    if [ "${confirm,,}" != "j" ]; then
        echo -e "${red}✘${reset} Abbruch durch Benutzer."
        exit 1
    fi
}

# Funktion, um den Benutzernamen abzufragen
get_username() {
    while true; do
        echo -n -e "${yellow}|${reset} Bitte Benutzernamen eingeben: "
        read -r username
        [ -n "$username" ] && break
        echo -e "${red}✘${reset} Ungültige Eingabe. Bitte versuche es erneut."
    done
}

# Funktion, um das Passwort abzufragen
get_password() {
    while true; do
        echo -n -e "${yellow}|${reset} Bitte Passwort eingeben: "
        read -rs password
        echo
        [ -n "$password" ] && break
        echo -e "${red}✘${reset} Ungültige Eingabe. Bitte versuche es erneut."
    done
}

# Funktion zum Überprüfen und Zuweisen von UID und GID
check_and_assign_uid_gid() {
    # Überprüfen, ob UID/GID bereits vergeben sind
    while id "$default_uid" &>/dev/null || grep -q ":$default_gid:" /etc/group; do
        ((default_uid++))
        ((default_gid++))
    done
}

# Funktion zum Erstellen von Systemgruppe und Systembenutzer
create_system_user_and_group() {
    # Gruppe erstellen
    sudo groupadd -r -g "$default_gid" "$username"

    # Benutzer erstellen
    sudo useradd -r -m -u "$default_uid" -g "$default_gid" -s /bin/zsh -c "Systemuser" "$username"
    echo "$username:$password" | sudo chpasswd
}

# Bestätigung für die Systemuser-Erstellung abfragen
confirm_creation

# Benutzernamen und Passwort abfragen
get_username
get_password

# Überprüfen und Zuweisen von UID und GID
check_and_assign_uid_gid

# Gruppennamen entspricht Benutzernamen
groupname="$username"

# Erstellen von Systemgruppe und Systembenutzer
create_system_user_and_group

# Erfolgsmeldung mit grünem Haken anzeigen
echo -e "${green}✔${reset} Serviceuser '${green}$username${reset}' und Servicegruppe '${green}$groupname${reset}' erfolgreich erstellt."
